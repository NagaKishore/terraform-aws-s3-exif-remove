# terraform-aws-s3-exif-remove

terraform-aws-s3-exif-remove

Preliminary development of terraform code to remove exif data from images in an S3 bucket, and move to another bucket.


```
cd existing_repo
git remote add origin https://gitlab.com/NagaKishore/terraform-aws-s3-exif-remove.git
git branch -M main
git push -uf origin main
```


## Execute manually in a terraform environment by using below commands. Change the names of S3 buckets in main.tf
```
terraform init
terraform plan
terraform apply
```


# Change the names of S3 buckets in main.tf (line 40) as required, and must be unique. 
