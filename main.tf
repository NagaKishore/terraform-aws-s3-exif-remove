########
# Main #
########

provider "aws" {
  region = "eu-west-2"
  skip_requesting_account_id  = true
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
}

# Create User A #

resource "aws_iam_user" "userA" {
  name = "userA"
  path = "/system/"

  tags = {
    tag-key = "readwrite-user"
  }
}

# Create User B #

resource "aws_iam_user" "userB" {
  name = "userB"
  path = "/system/"

  tags = {
    tag-key = "read-user"
  }
}

# Bucket Names A and B #

variable "s3_bucket_names" {
  type = list
  default = ["images-withexif-bucketa", "images-withoutexif-bucketb"]
}

# Create buckets from Bucket Names #
/* 
resource "aws_s3_bucket" "image_buckets" {
  count         = length(var.s3_bucket_names)
  bucket        = var.s3_bucket_names[count.index]
  acl           = "private"
  force_destroy = true
} */

resource "aws_s3_bucket" "images_withexif_bucketa" {
  bucket        = var.s3_bucket_names[0]
  acl           = "private"
  force_destroy = true
} 


resource "aws_s3_bucket" "images_withoutexif_bucketb" {
  bucket        = var.s3_bucket_names[1]
  acl           = "private"
  force_destroy = true
} 


# Create Bucket A policy Document #

data "aws_iam_policy_document" "s3_bucketa_read_write_policy_doc" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = [
      "arn:aws:s3:::images-withexif-bucketa"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject", "s3:PutObject",
      "s3:GetObjectVersion",
      "s3:GetBucketAcl",
      "s3:GetBucketLocation",
      "s3:PutBucketAcl"
    ]

    resources = [
      "arn:aws:s3:::images-withexif-bucketa/*"
    ]
  }
}

# Create Bucket A Read Write policy #

resource "aws_iam_policy" "s3_bucketa_read_write_policy" {
  name        = "S3BucketAReadWrite"
  description = "S3 Bucket A Read Write."
  policy      = data.aws_iam_policy_document.s3_bucketa_read_write_policy_doc.json
}


# Create Bucket B policy Document #

data "aws_iam_policy_document" "s3_bucketb_read_only_policy_doc" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = [
      "arn:aws:s3:::images-withoutexif-bucketb"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject"
    ]

    resources = [
      "arn:aws:s3:::images-withoutexif-bucketb/*"
    ]
  }
}

# Create Bucket B policy #

resource "aws_iam_policy" "s3_bucketb_read_only_policy" {
  name        = "S3BucketBReadOnly"
  description = "S3 Bucket B Read Only."
  policy      = data.aws_iam_policy_document.s3_bucketb_read_only_policy_doc.json
}

# Create IAM role for Bucket A read write #

resource "aws_iam_user_policy_attachment" "s3-readwrite-policy-attach" {
  user       = aws_iam_user.userA.name
  policy_arn = aws_iam_policy.s3_bucketa_read_write_policy.arn
}

resource "aws_iam_user_policy_attachment" "s3-readonly-policy-attach" {
  user       = aws_iam_user.userB.name
  policy_arn = aws_iam_policy.s3_bucketb_read_only_policy.arn
}




data "aws_iam_policy_document" "s3_lambda_read_write_policy_doc" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = [
      "arn:aws:s3:::images-withexif-bucketa",
      "arn:aws:s3:::images-withoutexif-bucketb"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:*"
    ]

    resources = [
      "arn:aws:s3:::images-withexif-bucketa/*",
      "arn:aws:s3:::images-withoutexif-bucketb/*"
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup"
    ]

    resources = [
      "*"
    ]
  }

    statement {
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
                "logs:PutLogEvents"
    ]

    resources = [
      "*"
    ]
  }

}

resource "aws_iam_policy" "s3_lambda_read_write_policy" {
  name        = "S3ReadWriteLambda"
  description = "S3 Read Write Lambda"
  policy      = data.aws_iam_policy_document.s3_lambda_read_write_policy_doc.json
}


resource "aws_iam_role" "s3_lambda_read_write_role" {
    name = "s3lambdareadwriterole"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
    tags = {
    name = "S3LambdaReadWriteRole"
  }
}

resource "aws_iam_role_policy_attachment" "s3_lambda_policy_attach" {
  role       = aws_iam_role.s3_lambda_read_write_role.name
  policy_arn = aws_iam_policy.s3_lambda_read_write_policy.arn
}


resource "aws_lambda_function" "lambda_exif_remove" {
  filename      = "moveimage.zip"
  # function_name = "lambda_exif_remove_function"
  function_name = "lambda_handler"
  # role = "arn:aws:iam::841586232130:role/service-role/moveimage-role-g9mynf2a"
  role          = aws_iam_role.s3_lambda_read_write_role.arn
  handler       = "lambda_function.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("moveimage.zip")

  runtime = "python3.9"

  environment {
    variables = {
      bucketB_name = var.s3_bucket_names[1]
    }
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_exif_remove.arn
  principal     = "s3.amazonaws.com"
  # source_arn    = "arn:aws:s3:::images-withexif-bucketa"
  source_arn =  aws_s3_bucket.images_withexif_bucketa.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.images_withexif_bucketa.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_exif_remove.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "images/"
    filter_suffix       = ".jpg"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
